package com.itau.Mercado.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.Mercado.model.Produtos;
import com.itau.Mercado.repository.ProdutosRepository;



@Controller
public class ProdutosController {
	
	@Autowired
	ProdutosRepository produtosRepository;

	@RequestMapping(path="/produtos", method = RequestMethod.POST)
	public @ResponseBody Produtos cadastrarProduto(@RequestBody Produtos produto) {
		return produtosRepository.save(produto);
	}
	
}
