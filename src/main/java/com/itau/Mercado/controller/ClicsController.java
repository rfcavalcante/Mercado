package com.itau.Mercado.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.Mercado.model.Clics;
import com.itau.Mercado.repository.ClicsRepository;

@Controller
public class ClicsController{
	@Autowired
	ClicsRepository clicsRepository;

	@RequestMapping(path="/", method = RequestMethod.POST)
	public @ResponseBody Clics cadastraClics(@RequestHeader HttpHeaders http) {
		Clics clic = new Clics();
		clic.setNavegador(HttpHeaders.USER_AGENT.split(" ")[0]);
		clic.setSO(HttpHeaders.USER_AGENT.split(" ")[1].substring(1));
		clic.setUrl(HttpHeaders.ORIGIN);
	
		System.out.println(http);
		
		return clicsRepository.save(clic);
		
	}
//	@RequestMapping(path="/", method = RequestMethod.POST)

}
