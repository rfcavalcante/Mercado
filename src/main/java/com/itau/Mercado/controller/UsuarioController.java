package com.itau.Mercado.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itau.Mercado.model.Produtos;
import com.itau.Mercado.model.Usuario;
import com.itau.Mercado.repository.ProdutosRepository;
import com.itau.Mercado.repository.UsuarioRepository;
import com.itau.Mercado.services.Criptografia;


@Controller
public class UsuarioController {
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	Criptografia passwordService;
	

	@RequestMapping(path="/usuariocadastrar", method = RequestMethod.POST)
	public @ResponseBody Usuario cadastrarUsuario(@RequestBody Usuario usuario) {
		
		String hash = passwordService.encode(usuario.getSenha());
		usuario.setSenha(hash);
		System.out.println("Senha hash:" + hash);
		
		return usuarioRepository.save(usuario);
	}
	
	@RequestMapping(path="/usuariovalidar", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> logar(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBanco = usuarioRepository.findByEmail(usuario.getEmail());	
		
		if(! usuarioBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		boolean deuCerto = passwordService.verificar(usuario.getSenha(), usuarioBanco.get().getSenha());
		
		if(deuCerto) {
			return ResponseEntity.ok(usuarioBanco);
		}
		
		return ResponseEntity.badRequest().build();
	}
}
