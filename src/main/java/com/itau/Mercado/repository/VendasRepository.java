package com.itau.Mercado.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.Mercado.model.Venda;

public interface VendasRepository extends CrudRepository<Venda, Long>{

}
	