package com.itau.Mercado.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.Mercado.model.Produtos;

public interface ProdutosRepository extends CrudRepository<Produtos, Integer>{

}
	