package com.itau.Mercado.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.Mercado.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{
	public Optional<Usuario> findByEmail(String email);
}
	