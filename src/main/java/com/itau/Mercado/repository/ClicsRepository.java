package com.itau.Mercado.repository;

import org.springframework.data.repository.CrudRepository;

import com.itau.Mercado.model.Clics;

public interface ClicsRepository extends CrudRepository<Clics, Integer>{

}
