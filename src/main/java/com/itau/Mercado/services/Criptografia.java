package com.itau.Mercado.services;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class Criptografia {


	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	public String encode(String senha) {
		return encoder.encode(senha);
	}

	public boolean verificar(String senha, String hash) {
		return encoder.matches(senha, hash);
	}
}


